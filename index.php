<?php 
    require 'api.php';
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Covid-19 RealTime</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Monitoring 
            <div class="row ">
                <div class="col text-center nav-item"><h6>Covid-19</h6></div>
            </div>    
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <!-- <span class="navbar-toggler-icon"></span> -->
        </button>
        <form class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Realtime Virus Covid-19 Indonesia</button>
        </form>
        
        </div>
    </nav>

    <div class="card">
        <div class="card-body text-center">
            <h2>Total Penyebaran Covid-19</h2>
        </div>
        <div class="card-body text-center">
            <div class="progress">
                <!-- <?php foreach($jumlah as $c_jumlah) :?> -->
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= count($jumlah)?></div>
                <!-- <?php endforeach;?> -->
            </div>
        </div>
    </div>
   

    <div class="card-columns">
        <?php foreach($kasus as $c_kasus):?>
        <div class="card text-center">
            <div class="card-body">
            <h5 class="card-title"><?=$c_kasus['kasus']?></h5>
            <p class="card-text"><span class="badge badge-pill badge-danger">Penyebab Penularan</span></p>
            <p class="card-text"><?= $c_kasus['penularan']?></p>
            <p class="card-text"><small class="text-muted">Pengumuman <span class="badge badge-info"><?= $c_kasus['pengumuman']?></span></small></p>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Detail
            </button>
            </div>
        </div>

          <!-- Modal -->
    
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail <?=$c_kasus['kasus']?> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Status &nbsp;&nbsp;<span class="badge badge-pill badge-success"><?=$c_kasus['status']?></span></p>
                <p>Klaster &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?=$c_kasus['klaster']?></span></p>
                <p>Jenis Kelamin &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?=$c_kasus['gender']?></span></p>
                <p>Umur &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?=$c_kasus['umurtext']?></span></p>
                <p>Rumah Sakit &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?=$c_kasus['rs']?></span></p>
                <p>Warga Negara &nbsp;&nbsp;<span class="badge badge-pill badge-primary"><?=$c_kasus['wn']?></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        
    </div>
        <?php endforeach;?>
    </div>

  
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>